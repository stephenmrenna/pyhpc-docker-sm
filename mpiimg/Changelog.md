## 0.2.4 (release date: 2018-11-04)

* Changed name to heponhpc/mpiimg, now based on heponhpc/pyimg:0.1.1

## 0.0.1 (release date: 2018-05-01)

* Initial release
