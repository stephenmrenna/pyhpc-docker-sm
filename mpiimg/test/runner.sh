die()
{
  local exitval
  if [[ "$1" =~ ^[0-9]*$ ]]; then (( exitval = $1 )); shift; else (( exitval = 1 )); fi
  echo "ERROR: $@" 1>&2
  exit $exitval
}

STD_ARGS="--rm -it --volume $PWD/test:/test --workdir /test"
IMAGE="${NAME}:${VERSION}"

#docker run ${STD_ARGS} ${IMAGE} bash -cl "python -V 2>& 1 | fgrep 3.6.1" || die "wrong version of python" 

docker run ${STD_ARGS} ${IMAGE} bash -cl "which mpicc" || die "mpicc not found" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "mpicc -dumpversion | fgrep 6.3.0" || die "wrong version of mpicc" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "mpif90 -dumpversion | fgrep 6.3.0" || die "wrong version of mpif90" 
docker run ${STD_ARGS} ${IMAGE} bash -cl 'python -c "from mpi4py import MPI" ' || die "mpi4py not installed" 
docker run ${STD_ARGS} ${IMAGE} bash -cl 'mpiexec -n 5 python -m mpi4py.bench helloworld ' || die "mpi4py not installed" 

echo "All tests OK"
