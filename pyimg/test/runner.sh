die()
{
  local exitval
  if [[ "$1" =~ ^[0-9]*$ ]]; then (( exitval = $1 )); shift; else (( exitval = 1 )); fi
  echo "ERROR: $@" 1>&2
  exit $exitval
}

STD_ARGS="--rm -it --volume $PWD/test:/test --workdir /test"
IMAGE="${NAME}:${VERSION}"

docker run ${STD_ARGS} ${IMAGE} bash -cl "python -V 2>& 1 | fgrep 3.6.1" || die "wrong version of python" 

docker run ${STD_ARGS} ${IMAGE} bash -cl "gcc -dumpversion | fgrep 6.3.0" || die "wrong version of gcc" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "g++ -dumpversion | fgrep 6.3.0" || die "wrong version of g++" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "gfortran -dumpversion | fgrep 6.3.0" || die "wrong version of gfortran" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "which h5ls" && die "h5ls is installed" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "python -c 'import numpy as np; np.lib.test()'" || die "numpy not installed" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "python -c 'import pandas'" && die "pandas is installed" 

#nose tests? 
echo "All tests OK"
