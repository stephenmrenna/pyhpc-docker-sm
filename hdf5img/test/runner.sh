die()
{
  local exitval
  if [[ "$1" =~ ^[0-9]*$ ]]; then (( exitval = $1 )); shift; else (( exitval = 1 )); fi
  echo "ERROR: $@" 1>&2
  exit $exitval
}

STD_ARGS="--rm -it --volume $PWD/test:/test --workdir /test"
IMAGE="${NAME}:${VERSION}"

docker run ${STD_ARGS} ${IMAGE} bash -cl "h5ls --version | grep 1.10.4" || die "h5ls not installed" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "python -c 'import h5py'" || die "h5py not installed" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "python -c 'import h5py; assert h5py.get_config().mpi'" || die "h5py not installed with mpi" 

#nose tests? 
echo "All tests OK"
