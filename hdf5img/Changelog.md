## 0.3.0 (release date: 2018-11-04)

* Move to heponhpc/hdf5img
* Update HDF5 to version 1.10.4
* Update cython to 0.29.0
* Update h5py to 2.8.0, and get it from GitHub rather than PythonHosted


## 0.0.1 (release date: 2018-05-01)

* Initial release
