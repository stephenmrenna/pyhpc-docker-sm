## 0.2.0 (release date: 2018-11-05)

* Add tables 3.4.4, and dependencies.

## 0.1.0 (release date: 2018-11-04)

* Update to pandas 0.23.4

## 0.0.1 (release date: 2018-09-27)

* Initial release
