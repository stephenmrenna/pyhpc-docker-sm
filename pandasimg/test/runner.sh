die()
{
  local exitval
  if [[ "$1" =~ ^[0-9]*$ ]]; then (( exitval = $1 )); shift; else (( exitval = 1 )); fi
  echo "ERROR: $@" 1>&2
  exit $exitval
}

STD_ARGS="--rm -it --volume $PWD/test:/test --workdir /test"
IMAGE="${NAME}:${VERSION}"

docker run ${STD_ARGS} ${IMAGE} bash -cl "python -c 'import pandas; pandas.test()'" || die "pandas not installed"
docker run ${STD_ARGS} ${IMAGE} bash -cl "python -c 'import tables; tables.test()'" || die "tables not installed"

#nose tests? 
echo "All tests OK"
